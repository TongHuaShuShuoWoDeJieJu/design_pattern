package com.ghy.demo18.lnterpreteroattern.figure;

//抽象表达式角色
public interface IArithmeticInterpreter {
    int interpret();
}
