package com.ghy.demo18.lnterpreteroattern.figure;

//创建数字表达式类
public class NumInterpreter implements IArithmeticInterpreter {
    private int value;

    public NumInterpreter(int value) {
        this.value = value;
    }


    public int interpret() {
        return this.value;
    }
}
