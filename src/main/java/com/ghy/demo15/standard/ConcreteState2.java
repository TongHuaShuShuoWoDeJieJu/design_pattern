package com.ghy.demo15.standard;


public class ConcreteState2 extends State {

    @Override
    public void handle1() {
        //...
        System.out.println("ConcreteState2 的 handle2 方法");
    }

    @Override
    public void handle2() {
        super.context.setCurrentState(Context.STATE1);
        System.out.println("ConcreteState2的 handle2 方法");
    }

}
