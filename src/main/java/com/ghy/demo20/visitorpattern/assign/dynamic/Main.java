package com.ghy.demo20.visitorpattern.assign.dynamic;


public class Main {
    public static void main(String[] args) {
        Person man = new Man();
        Person woman = new WoMan();

        man.test();
        woman.test();
    }
}
