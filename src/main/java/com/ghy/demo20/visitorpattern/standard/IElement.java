package com.ghy.demo20.visitorpattern.standard;


// 抽象元素
public interface IElement {
    void accept(IVisitor visitor);
}
