package com.ghy.demo17.mediatorpattern.chat;


public class Test {
    public static void main(String[] args) {
        ChatRoom chatRoom = new ChatRoom();

        User zs = new User("张三",chatRoom);
        User ls = new User("李四",chatRoom);

        zs.sendMessage("您好");
        ls.sendMessage("有啥事");
    }
}
