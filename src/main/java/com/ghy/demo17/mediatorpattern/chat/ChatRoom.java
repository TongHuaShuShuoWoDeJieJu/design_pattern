package com.ghy.demo17.mediatorpattern.chat;

//聊天室
public class ChatRoom {
    public void showMsg(User user,String msg){
        System.out.println("[" + user.getName() + "] : " + msg);
    }
}
