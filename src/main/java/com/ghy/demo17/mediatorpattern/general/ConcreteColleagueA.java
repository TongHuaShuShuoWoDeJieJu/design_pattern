package com.ghy.demo17.mediatorpattern.general;

// 具体同事类
public class ConcreteColleagueA extends Colleague {
    public ConcreteColleagueA(Mediator mediator) {
        super(mediator);
        this.mediator.setColleageA(this);
    }

    // 自有方法
    public void selfMethodA() {
        // 处理自己的逻辑
        System.out.println(String.format("自有方法", this.getClass().getSimpleName()));
    }

    // 依赖方法
    public void depMethodA() {
        // 处理自己的逻辑
        System.out.println(String.format("依赖方法", this.getClass().getSimpleName()));
        // 无法处理的业务逻辑委托给中介者处理
        this.mediator.transferA();
    }
}
