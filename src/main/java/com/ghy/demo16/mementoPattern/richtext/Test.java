package com.ghy.demo16.mementoPattern.richtext;


public class Test {
    public static void main(String[] args) {
    DraftsBox draftsBox = new DraftsBox();
    Editor editor = new Editor("备忘录模式",
            "备忘录模式(Memento Pattern) 又称为快照模式(Snapshot Pattern) 或令牌模式(Token Pattern) ",
            "1.png");

        ArticleMemento articleMemento = editor.saveToMemento();
        draftsBox.addMemento(articleMemento);

        System.out.println("标题：" + editor.getTitle() + "\n" +
                "内容：" + editor.getContent() + "\n" +
                "插图：" + editor.getImgs() + "\n暂存成功");

        System.out.println("完整的信息" + editor);


        System.out.println("==========第一次修改===========");
        editor.setTitle("备忘录模式");
        editor.setContent("备忘录模式(Memento Pattern) 又称为快照模式(Snapshot Pattern) 或令牌模式(Token Pattern) ， 是指在不破坏封装的前提下");

        System.out.println("===========第一次修改文章完成===========");

        System.out.println("完整的信息" + editor);

        articleMemento = editor.saveToMemento();
        draftsBox.addMemento(articleMemento);

        System.out.println("==========保存到草稿箱===========");


        System.out.println("==========第2次修改文章===========");
        editor.setTitle("备忘录模式1");
        editor.setContent("备忘录模式(Memento Pattern) 又称为快照模式(Snapshot Pattern) 或令牌模式(Token Pattern) ， 是指在不破坏封装的前提下， 捕获一个对象的内部状态，");
        System.out.println("完整的信息" + editor);
        System.out.println("==========第2次修改文章完成===========");

        System.out.println("==========第1次撤销===========");
       articleMemento = draftsBox.getMemento();
        editor.undoFromMemento(articleMemento);
        System.out.println("完整的信息" + editor);
        System.out.println("==========第1次撤销完成===========");


        System.out.println("==========第2次撤销===========");
    articleMemento = draftsBox.getMemento();
        editor.undoFromMemento(articleMemento);
        System.out.println("完整的信息" + editor);
        System.out.println("==========第2次撤销完成===========");

}
}
