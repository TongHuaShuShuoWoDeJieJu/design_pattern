package com.ghy.demo16.mementoPattern.richtext;

import java.util.Stack;

//备忘录管理员角色(Caretaker):草稿箱
public class DraftsBox {
    //采用容器，stack采用了后进先出的逻辑，有兴趣的可以看源码，很简单
    private final Stack<ArticleMemento> STACK = new Stack<ArticleMemento>();
   //取栈
    public ArticleMemento getMemento(){
        ArticleMemento articleMemento = STACK.pop();
        return articleMemento;
    }
   //压栈
    public void addMemento(ArticleMemento articleMemento){
        STACK.push(articleMemento);
    }
}
