package com.ghy.demo19.observer.jdk;


public class Test {
    public static void main(String[] args) {
        Bloggarden bloggarden = Bloggarden.getInstance();
        Teacher zs = new Teacher("张三");
        Teacher ls = new Teacher("李四");

        bloggarden.addObserver(zs);
        bloggarden.addObserver(ls);

        //用户行为
        Question question = new Question();
        question.setUserName("张三");
        question.setContent("观察者模式的源码应用场景有哪些");

        bloggarden.publishQuestion(question);
    }
}
