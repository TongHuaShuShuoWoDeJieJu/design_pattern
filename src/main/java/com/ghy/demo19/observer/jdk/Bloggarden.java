package com.ghy.demo19.observer.jdk;

import java.util.Observable;

//采用JDK现有的轮子Observable
public class Bloggarden extends Observable {
    private String name = "博客园";
    private static final Bloggarden bloggarden = new Bloggarden();
    private Bloggarden() {}

    public static Bloggarden getInstance(){
        return bloggarden;
    }
    public String getName() {
        return name;
    }
    public void publishQuestion(Question question){
        System.out.println(question.getUserName() + "在" + this.name + "上提交了一个问题。");
        setChanged();
        notifyObservers(question);
    }
}
