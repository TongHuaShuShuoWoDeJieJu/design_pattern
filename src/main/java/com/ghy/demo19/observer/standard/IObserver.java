package com.ghy.demo19.observer.standard;

//抽象观察者
public interface IObserver<E> {
    void update(E event);
}
