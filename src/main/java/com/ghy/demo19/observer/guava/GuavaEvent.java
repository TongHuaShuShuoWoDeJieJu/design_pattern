package com.ghy.demo19.observer.guava;

import com.google.common.eventbus.Subscribe;

//定义观察者，Guava是通过注解实现的
public class GuavaEvent {

    @Subscribe
    public void observer(String str){

        System.out.println("执行observer方法，传参为：" + str);

    }
}
