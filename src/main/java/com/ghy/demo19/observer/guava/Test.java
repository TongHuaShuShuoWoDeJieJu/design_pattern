package com.ghy.demo19.observer.guava;


import com.google.common.eventbus.EventBus;

public class Test {
    public static void main(String[] args) {
        //Guava的API，消息总线
        EventBus eventBus = new EventBus();

        GuavaEvent guavaEvent = new GuavaEvent();
        //将guavaEvent注册到消息总线中去
        eventBus.register(guavaEvent);

       //发送消息
        eventBus.post("来自总线发送的消息");
    }
}
