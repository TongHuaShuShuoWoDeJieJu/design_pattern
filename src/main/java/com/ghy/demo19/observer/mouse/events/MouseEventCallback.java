package com.ghy.demo19.observer.mouse.events;

import com.ghy.demo19.observer.mouse.core.Event;

//观察者
public class MouseEventCallback {
    public void  onClick(Event e){
        System.out.println("触发鼠标单击事件");
    }
}
