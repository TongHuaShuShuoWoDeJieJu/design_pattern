package com.ghy.demo19.observer.mouse;


import com.ghy.demo19.observer.mouse.events.Mouse;
import com.ghy.demo19.observer.mouse.events.MouseEventCallback;
import com.ghy.demo19.observer.mouse.events.MouseEventType;

public class Test {
    public static void main(String[] args) {
        MouseEventCallback callback = new MouseEventCallback();


        Mouse mouse = new Mouse();
        mouse.addLisenter(MouseEventType.ON_CLICK,callback);
        mouse.addLisenter(MouseEventType.ON_MOVE,callback);

        mouse.click();
        mouse.move();
    }
}
